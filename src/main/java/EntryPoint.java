import utils.SparkMain;
import utils.Stats;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EntryPoint {


    public static void main(String args[]){
        List<String> files = null;
        try(Stream<Path> paths = Files.walk(Paths.get("/app/projekt/pliki/"))){
            files = paths.map(p -> p.toString()).filter(p -> p.endsWith(".log")).collect(Collectors.toList());
            if(files.size() == 0)
                return;
            SparkMain sparkMain = new SparkMain();
            ArrayList<Stats> statsList = new ArrayList<>();
            for (String file : files) {
                statsList.add(sparkMain.beginProcess(file));
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
            File file = new File("mkdir /app/projekt/logi/" + formatter.format(LocalDateTime.now()) + ".txt");
            if(file.createNewFile()){
                FileWriter writer = new FileWriter(file);
                for(Stats stat : statsList){
                    writer.write(stat.toString());
                    writer.write("\n");
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
