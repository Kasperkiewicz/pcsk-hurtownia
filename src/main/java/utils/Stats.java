package utils;

public class Stats {
    private String fileName;
    private long pre;
    private long after;

    public Stats(String fileName, long pre,long after){
        this.fileName = fileName;
        this.pre = pre;
        this.after = after;
    }

    @Override
    public String toString(){
        float percent = 0.0f;
        if(pre != 0l){
            percent = (float)(after/pre)*100.0f;
        }
        return fileName + " before : " + pre + "  after : " + after + " percentage of valuable data : " + percent;
    }
}
