package utils;

import org.apache.spark.SparkConf;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.io.File;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.apache.spark.sql.functions.*;

public class SparkMain {
    private SparkConf config;
    private SparkSession session;
    private Dataset<Row> dataset;


    public SparkMain() {
        config = new SparkConf().setMaster("local[*]").setAppName("ZBDIH").set("spark.cassandra.connection.host", "127.0.0.1")
        .set("spark.cassandra.connection.port", "9042");
        session = SparkSession.builder().config(config).appName("ZBDIH").getOrCreate();
    }

    public Stats beginProcess(String path) {
        if (!new File(new String(path).trim()).isFile()) {
            System.out.println("File doesn't exist");
            return new Stats(path, 0,0);
        }

        Dataset<Row> dts = session.read().format("text").load(path);
        Dataset<Row> userAddDS = parseAddUser(dts);
        long linesPre = dts.count();
        long linesAfter = userAddDS.count();
        userAddDS.show();
        Dataset<Row> userDelDS = parseDelUser(dts);
        linesAfter += userDelDS.count();
        userDelDS.show();
        /*Dataset<Row> applicationDs = parseApplication(dts);
        applicationDs.show();
        linesAfter += applicationDs.count();
        Dataset<Row> modificationDs = parseModification(dts);
        modificationDs.show();
        linesAfter += modificationDs.count();
        Dataset<Row> auditDs = parseAuditr(dts);
        auditDs.show();
        linesAfter += auditDs.count();*/
        saveToCassandra(userAddDS, "new_user");
        saveToCassandra(userDelDS, "del_user");
        //saveToCassandra(applicationDs, "application");
        //saveToCassandra(auditDs, "auditr");
        //saveToCassandra(modificationDs, "modification");
        return  new Stats(path, linesPre, linesAfter);
    }
        private void saveToCassandra(Dataset<Row> dataset, String table){
        dataset.write().format("org.apache.spark.sql.cassandra").
                option("table", table).option("keyspace","dba").
                mode(SaveMode.Append).save();
    }

    private Dataset<Row> parseAddUser(Dataset<Row> dataset) {
        return dataset.filter(col("value").contains("type=ADD_USER").and(col("value").
                contains("res=success"))).select(
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+id\\=(?<userid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 1).as("actionname"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+id\\=(?<userid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 2).as("created"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+id\\=(?<userid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 3).as("userid"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+id\\=(?<userid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 4).as("hostname"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+id\\=(?<userid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 5).as("result")
        ).withColumn("actionid", col("created").cast(DataTypes.IntegerType))
                .withColumn("created", from_unixtime(col("created")))
                .withColumn("userid", col("userid").cast(DataTypes.IntegerType));
    }

    private Dataset<Row> parseDelUser(Dataset<Row> dataset) {
        return dataset.filter(col("value").contains("type=DEL_USER").and(col("value").
                contains("res=success"))).select(
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<deleted>\\d+)\\..+\\).+id\\=(?<usrid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 1).as("actionname"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<deleted>\\d+)\\..+\\).+id\\=(?<usrid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 2).as("deleted"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<deleted>\\d+)\\..+\\).+id\\=(?<usrid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 3).as("userid"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<deleted>\\d+)\\..+\\).+id\\=(?<usrid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 4).as("hostname"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<deleted>\\d+)\\..+\\).+id\\=(?<usrid>\\w+).+hostname\\=(?<hostname>\\S+).+res\\=(?<result>\\w+)\\'", 5).as("result")
        ).withColumn("actionid", col("deleted").cast(DataTypes.IntegerType))
                .withColumn("deleted", from_unixtime(col("deleted")));
    }

    private Dataset<Row> parseAuditr(Dataset<Row> dataset) {
        return dataset.filter(col("value").contains("type=CONFIG_CHANGE")).select(
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+).+op=(?<action>\\S+)\\s+key=\"(?<module>\\S+)\\\".+res=(?<result>\\S+)\n", 1).as("actionname"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+).+op=(?<action>\\S+)\\s+key=\"(?<module>\\S+)\\\".+res=(?<result>\\S+)\n", 2).as("created"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+).+op=(?<action>\\S+)\\s+key=\"(?<module>\\S+)\\\".+res=(?<result>\\S+)\n", 3).as("action"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+).+op=(?<action>\\S+)\\s+key=\"(?<module>\\S+)\\\".+res=(?<result>\\S+)\n", 4).as("module"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+).+op=(?<action>\\S+)\\s+key=\"(?<module>\\S+)\\\".+res=(?<result>\\S+)\n", 5).as("result")
        ).withColumn("actionid", col("created").cast(DataTypes.IntegerType))
                .withColumn("created", from_unixtime(col("created")));
    }

    private Dataset<Row> parseApplication(Dataset<Row> dataset) {
        return dataset.filter(col("value").contains("type=EXECVE").and(col("value").contains("apt-get"))
        ).select(
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\d+)\\..+\\).+a0=\\\"(?<menpak>\\S+)\\\"\\s+a1=\"(?<action>\\S+)\\\"\\s+a2=\\\"(?<applicationname>\\S+)\\\"\n", 1).as("action"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\d+)\\..+\\).+a0=\\\"(?<menpak>\\S+)\\\"\\s+a1=\"(?<action>\\S+)\\\"\\s+a2=\\\"(?<applicationname>\\S+)\\\"\n", 2).as("actionid"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\d+)\\..+\\).+a0=\\\"(?<menpak>\\S+)\\\"\\s+a1=\"(?<action>\\S+)\\\"\\s+a2=\\\"(?<applicationname>\\S+)\\\"\n", 3).as("men_pak"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\d+)\\..+\\).+a0=\\\"(?<menpak>\\S+)\\\"\\s+a1=\"(?<action>\\S+)\\\"\\s+a2=\\\"(?<applicationname>\\S+)\\\"\n", 4).as("application_name"));
    }

    private Dataset<Row> parseModification(Dataset<Row> dataset) {
        return dataset.filter(col("value").contains("type=EXECVE").and(col("value").
                contains("res=1"))).select(
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+a0=\\\"\\/bin\\/sh\\\"\\s+a1=\"(?<tool>\\S+)\\\"\\s+a2=\\\"(?<source>\\S+)\\\"", 1).as("actionname"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+a0=\\\"\\/bin\\/sh\\\"\\s+a1=\"(?<tool>\\S+)\\\"\\s+a2=\\\"(?<source>\\S+)\\\"", 2).as("created"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+a0=\\\"\\/bin\\/sh\\\"\\s+a1=\"(?<tool>\\S+)\\\"\\s+a2=\\\"(?<source>\\S+)\\\"", 3).as("tool"),
                regexp_extract(col("value"), "type\\=(?<actionname>\\S+)\\s+msg\\=audit\\((?<created>\\w+)\\..+\\).+a0=\\\"\\/bin\\/sh\\\"\\s+a1=\"(?<tool>\\S+)\\\"\\s+a2=\\\"(?<source>\\S+)\\\"", 4).as("source")
        ).withColumn("actionid", col("created").cast(DataTypes.IntegerType))
                .withColumn("created", from_unixtime(col("created")));
    }
}
