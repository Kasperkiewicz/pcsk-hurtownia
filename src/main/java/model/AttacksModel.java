package model;

import java.io.Serializable;

public class AttacksModel implements Serializable {
    private String status;
    private String method;
    private String userAgent;
    private String pragma;
    private String cacheControl;
    private String accept;
    private String acceptEncoding;
    private String acceptCharset;
    private String cookie;
    private String contentType;
    private String connection;
    private String lenght;
    private String content;
    private String classification;
    private String url;

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public String getMethod() { return method; }

    public void setMethod(String method) { this.method = method; }

    public String getUserAgent() { return userAgent; }

    public void setUserAgent(String userAgent) { this.userAgent = userAgent; }

    public String getPragma() { return pragma; }

    public void setPragma(String pragma) { this.pragma = pragma; }

    public String getCacheControl() { return cacheControl; }

    public void setCacheControl(String cacheControl) { this.cacheControl = cacheControl; }

    public String getAccept() { return accept; }

    public void setAccept(String accept) { this.accept = accept; }

    public String getAcceptEncoding() { return acceptEncoding; }

    public void setAcceptEncoding(String acceptEncoding) { this.acceptEncoding = acceptEncoding; }

    public String getAcceptCharset() { return acceptCharset; }

    public void setAcceptCharset(String acceptCharset) { this.acceptCharset = acceptCharset; }

    public String getCookie() { return cookie; }

    public void setCookie(String cookie) { this.cookie = cookie; }

    public String getContentType() { return contentType; }

    public void setContentType(String contentType) { this.contentType = contentType; }

    public String getConnection() { return connection; }

    public void setConnection(String connection) { this.connection = connection; }

    public String getLenght() { return lenght; }

    public void setLenght(String lenght) { this.lenght = lenght; }

    public String getContent() { return content; }

    public void setContent(String content) { this.content = content; }

    public String getClassification() { return classification; }

    public void setClassification(String classification) { this.classification = classification; }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }
}
