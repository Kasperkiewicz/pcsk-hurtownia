package model;

import java.io.Serializable;
import java.math.BigInteger;

public class StatsModel implements Serializable {
    private BigInteger rows;
    private String type;
    private boolean withOptimalisation;
    private BigInteger timeToWrite;

    public BigInteger getRows() {
        return rows;
    }

    public void setRows(BigInteger rows) {
        this.rows = rows;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isWithOptimalisation() {
        return withOptimalisation;
    }

    public void setWithOptimalisation(boolean withOptimalisation) {
        this.withOptimalisation = withOptimalisation;
    }

    public BigInteger getTimeToWrite() {
        return timeToWrite;
    }

    public void setTimeToWrite(BigInteger timeToWrite) {
        this.timeToWrite = timeToWrite;
    }
}
